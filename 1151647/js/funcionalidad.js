var mesesPeriodo = [];

function ingresarDatos() {
    var cant = 12;
    var campos = document.getElementById("ingresar");
    var form = '<div class="form-group">';
    for (var i = 1; i <= cant; i++) {
        form += '<div id="formulario">';
        form += '<div><label>Digite las ventas del mes ' + i + ':</label></div>';
        form += '<div><input class="form-control" type="number" id="mes' + i + '" name="valor"></div>';
        form += '</div> <br>';
    }
    form += '<button type="button" class="btn btn-primary" onclick="datosGrafico()"> Crear Gráfico de Barras</button>';
    form += '</div>';
    campos.innerHTML = form;
}

function validar(anio, periodo, valores) {
    var mensaje = '';
    if (anio > 2020 || anio < 1970 || anio == '')
        mensaje = "El año ingresado es incorrecto";
    else if (periodo == '0')
        mensaje = "Debe seleccionar un periodo de análisis";
    else {
        for (var i = 0; i < valores.length; i++) {
            if (valores[i].value < 0 || valores[i].value == '') {
                mensaje = "Registre correctamente las ventas";
                i = valores.length;
            }
        }
    }
    return mensaje;
}

function cantidadMeses(periodo) {
    return 12 / periodo;
}

function procesarDatos(valores, cantMeses, mes, j, i) {
    meses = '';
    val = 0;

    for (; j < cantMeses; j++) {
        if (j + 1 == cantMeses) meses += mes[j];
        else meses += mes[j] + ' - ';
        val += parseInt(valores[j].value, 10);
    }
    mesesPeriodo[i] = meses;
    return val;
}

function obtenerVentas(periodo, anio, valores, meses) {
    var datos = [];
    mesesPeriodo = [];
    datos[0] = ["Año " + anio + " - Informe de Ventas " + periodo[1], "Ventas"];
    var p = periodo[0];
    if (p == 5) p++;
    var cant = cantidadMeses(p);

    for (var i = 0; i < cant; i++) {
        var aux = procesarDatos(valores, p * (i + 1), meses, p * i, i);
        datos[i + 1] = [i + 1, aux];
    }
    return datos;
}

function datosGrafico() {
    var mes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    var valores = document.getElementsByName("valor");
    var anio = document.getElementById("anio").value;
    var select = document.getElementById("periodo");
    var periodo = [select.value, select.options[select.value].text];

    msj = validar(anio, periodo[0], valores);
    if (msj != '')
        alert(msj);
    else {
        var cadena = obtenerVentas(periodo, anio, valores, mes);
        drawChart(anio, periodo, cadena);
        dibujarTabla(anio, cadena, periodo[0]);
    }
}

function drawChart(anio, periodo, datos) {
    alert("Se realizará el diagrama de barras " + periodo[1] + " del año " + anio);
    var data = new google.visualization.arrayToDataTable(datos);
    var options = {
        chart: {
            title: 'Informe de Ventas',
            subtitle: 'Año: ' + anio,
        },
        bars: 'horizontal'
    };
    var chart = new google.charts.Bar(document.getElementById('graficaBarras'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
}

function dibujarTabla(anio, datos, periodo) {
    var tabla = document.getElementById("tabla");
    var periodos = ['Número de Mes', 'Bimestre', 'Trimestre', 'Cuatrimestre', 'Semestre'];
    var cant = cantidadMeses(periodo);
    var estructura = "<table class='table table-striped'>";
    estructura += "<tr><th colspan='3'>Información de Ventas - Año: " + anio + "</th></tr>";
    estructura += "<tr> <th>" + periodos[periodo - 1] + "</th>";
    estructura += "<th> Meses </th>";
    estructura += "<th> Valor </th> </tr>";

    for (var i = 1; i <= cant; i++) {
        estructura += "<tr> <th>" + (i) + "</th>";
        estructura += "<th>" + mesesPeriodo[i - 1] + "</th>";
        estructura += "<th> $" + parseInt(datos[i][1], 10) + "</th> </tr>";
    }

    estructura += "</table>";
    tabla.innerHTML = estructura;
}